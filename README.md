# VRF Concierge

## An Atlas Corporation Chainlink Hackathon Submission

The purpose of this application is to provide notification services to users of chainlink VRF when their LINK balance is low. Through a GUI (forthcoming), a user can provide information on:
* Their subscription ID, and the chain they want to monitor. The app currently covers Eth Mainnet, Rinkeby, Polygon, Mumbai Testnet, BNB, and BNB Testnet.
* Their cell phone number to receive text notifications, email to receive email notifications, or Telegram to receive push notifications.
* [coming soon] The balance threshold of LINK that they wish to receive alerts for when the balance drops below.

## Deployment

`docker-compose build`

`docker-compose up`

visit localhost:1880 (0.0.0.0:8080) in a browser to view/edit node-red GUI/control panel. 

## Requirements

> You may need to add Twilio credentials. 
> An external mongo database can be configured but this deployment comes with a local.
